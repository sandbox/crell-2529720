<?php

namespace Drupal\ctools;

use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\EntityRouteProviderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class EntityConditionFormRouteProvider implements EntityRouteProviderInterface {
  use StringTranslationTrait;

  /**
   * Provides routes for entities.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type
   *
   * @return \Symfony\Component\Routing\RouteCollection|\Symfony\Component\Routing\Route[]
   *   Returns a route collection or an array of routes keyed by name, like
   *   route_callbacks inside 'routing.yml' files.
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = new RouteCollection();

    // Prevent this class from being used on content entities, since that makes
    // no sense.
    if (! $entity_type instanceof ConfigEntityTypeInterface) {
      return [];
    }

    $entity_type_id = $entity_type->id();

    if ($this->getConditionAdminKey($entity_type, 'list') && $entity_type->hasLinkTemplate('conditions-form')) {
      if ($route = $this->getConditionsListRoute($entity_type)) {
        $collection->add("entity.{$entity_type_id}.conditions", $route);
      }
      if ($route = $this->getConditionAddRoute($entity_type)) {
        $collection->add("entity.{$entity_type_id}.conditions.add", $route);
      }
      if ($route = $this->getConditionEditRoute($entity_type)) {
        $collection->add("entity.{$entity_type_id}.conditions.edit", $route);
      }
      if ($route = $this->getConditionDeleteRoute($entity_type)) {
        $collection->add("entity.{$entity_type_id}.conditions.delete", $route);
      }
    }

    return $collection;
  }

  protected function getConditionAdminKey(EntityTypeInterface $entity_type, $page) {
    $handlers = $entity_type->get('handlers');

    return isset($handlers['conditions_admin'][$page]) ? $handlers['conditions_admin'][$page] : NULL;
  }

  protected function getConditionsListRoute(EntityTypeInterface $entity_type) {
    $entity_type_id = $entity_type->id();

    $route = new Route($entity_type->getLinkTemplate('conditions-form'));

    $route
      ->addDefaults([
        '_form' => $this->getConditionAdminKey($entity_type, 'list'),
        '_title' => $this->t('Conditions'),
      ])
      ->setRequirement('_entity_access', "{$entity_type_id}.create")
      ->setOption('parameters', [
        $entity_type_id => ['type' => 'entity:' . $entity_type_id],
      ]);

    return $route;
  }

  protected function getConditionAddRoute(EntityTypeInterface $entity_type) {
    $entity_type_id = $entity_type->id();

    $route = new Route($entity_type->getLinkTemplate('conditions-form') . '/add');
    $route
      ->addDefaults([
        '_form' => $this->getConditionAdminKey($entity_type, 'edit'),
        '_title' => $this->t('Add Condition'),
      ])
      ->setRequirement('_entity_access', "{$entity_type_id}.create")
      ->setOption('parameters', [
        $entity_type_id => ['type' => 'entity:' . $entity_type_id],
      ]);

    return $route;
  }

  protected function getConditionEditRoute(EntityTypeInterface $entity_type) {
    $entity_type_id = $entity_type->id();

    $route = new Route($entity_type->getLinkTemplate('conditions-form') . '/edit');
    $route
      ->addDefaults([
        '_form' => $this->getConditionAdminKey($entity_type, 'edit'),
        '_title' => $this->t('Edit Condition'),
      ])
      ->setRequirement('_entity_access', "{$entity_type_id}.edit")
      ->setOption('parameters', [
        $entity_type_id => ['type' => 'entity:' . $entity_type_id],
      ]);

    return $route;
  }

  protected function getConditionDeleteRoute(EntityTypeInterface $entity_type) {
    $entity_type_id = $entity_type->id();

    $route = new Route($entity_type->getLinkTemplate('conditions-form') . '/delete');
    $route
      ->addDefaults([
        '_form' => $this->getConditionAdminKey($entity_type, 'delete'),
        '_title' => $this->t('Delete Condition'),
      ])
      ->setRequirement('_entity_access', "{$entity_type_id}.delete")
      ->setOption('parameters', [
        $entity_type_id => ['type' => 'entity:' . $entity_type_id],
      ]);

    return $route;
  }

}
