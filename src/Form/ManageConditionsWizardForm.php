<?php
/**
 * @file Contains \Drupal\ctools\Form\ManageConditionsWizardForm.
 */

namespace Drupal\ctools\Form;


abstract class ManageConditionsWizardForm extends ManageConditions {

  /**
   * Provide the tempstore id for your specified use case.
   *
   * @return string
   */
  abstract protected function getTempstoreId();

  /**
   * Document the route name and parameters for edit/delete context operations.
   *
   * The route name returned from this method is used as a "base" to which
   * ".edit" and ".delete" are appeneded in the getOperations() method.
   * Subclassing '\Drupal\ctools\Form\ConditionConfigure' and
   * '\Drupal\ctools\Form\ConditionDelete' should set you up for using this
   * approach quite seamlessly.
   *
   * @return array
   *   In the format of
   *   return ['route.base.name', ['machine_name' => $machine_name, 'context' => $row]];
   */
  abstract protected function getOperationsRouteInfo($machine_name, $row);

}
